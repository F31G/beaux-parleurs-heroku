<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;

class ImprovisationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $regard = Http::get('http://www.omdbapi.com/?s=regard&type=movie&apikey=f084b538');
            foreach($regard['Search'] as $film){
                $recherche = Http::get('http://www.omdbapi.com/?i='.$film['imdbID'].'&apikey=f084b538');

                
        // if pour prendre seulement les "id" avec un "plot"
         if($recherche['Plot']!="N/A"){

            DB::table('improvisations')->insert([

                
                'text' => $recherche['Plot']

            
                // 'text' => $recherche['Plot']!="N/A"
                // 'imdb_ID' => $recherche['imdbID']

            ]); 
        }   
    }
}
}