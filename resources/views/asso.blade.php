@include('parts.header')


<h1 class="text-center p-4">Bienvenue chez les "Beaux Parleurs"</h1>

<div class="p-4">
<h3 class="text-center">Présentation de la Compagnie</h3>
<br/>
    <p class="text-center">
        La "société des beaux-parleurs" est un club d'improvisation théatrale, dont les membres se réunissent chaque jeudi 
        soir pour s'adonner à leur pratique préférée. 
        <br/>
        L'impro théatrale se déroule comme suit, sous forme de "battles" : 
        <br/>
        l'arbitre au centre, et deux équipes de 2 ou 3 personnes de chaque côté. L'arbitre annonce un sujet, par exemple :
    </p>
    <p class="text-center">
        Deux familles se croisent au concours de barbecue de Dunkerque
    </p>
    <p class="text-center">
        Les deux teams commencent alors à improviser. Parfois, l'arbitre mentionne une contrainte supplémentaire : l'impro se 
        déroule en language "gromlo".
    </p>
    <p class="text-center">
        À la fin du battle, une équipe est déclarée gagnante par l'arbitre, qui rend son verdict à l'applaudimètre : en invitant 
        le public à faire un maximum de bruit pour l'équipe qui l'a le plus convaincu !
    </p>
    </div>

@include('parts.footer')