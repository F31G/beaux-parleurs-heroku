<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Improvisation;

class ImproController extends Controller
{

    function showRoulette(){
        return view('roulette',['improvisations' => Improvisation::where('available', 1 )->inRandomOrder()->first()]);
    }

    public function nouvelleMeilleure(Request $request)
    {
        $toujoursElle = $request->input("proposition");
        // dd($toujoursElle);
        $ajoutSujet = new Improvisation;
        $ajoutSujet->text = $toujoursElle;
        $ajoutSujet->save();
        return view('nouveauSujet');
    }



    // function ceQuonVeut(){
    //     $bla= "UnTrucBien";
    //     dd($bla);
    // }
}