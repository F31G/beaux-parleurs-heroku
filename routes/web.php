<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\ImproController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
});


Route::get('/asso', function () {
    return view('asso');
});

Route::get('/home', function () {
    return view('home');
});

Route::get('/roulette', [ImproController::class,'showRoulette']);

Route::get('/nouveausujet', function () {
    return view('nouveausujet');
});

Route::get('/meilleure', [ImproController::class,'nouvelleMeilleure'])->name('meilleure');